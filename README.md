# Conway's Game of Life
### Multithreaded with pthreads
For this project, I used the pthread thread library to implement a parallel version of Conway�s Game of Life. 

The computation can be parallelized in two ways: one partitions the board�s rows across the threads, the other partitions the boards columns across threads. 

After initializing the game board, the main thread spawns off worker threads that will play multiple rounds of the Game of Life, each thread computing just its portion of cells and the new board each round. Grid cells are allocated using either a row-wise or column-wise partitioning specified by a command line argument. One thread is responsible for printing the board after each round 

The number of threads, and the partition is specified by the command line (i.e. Usage ./gol board-configuration.txt num_threads partition[0:1] print[0:1]), where partition = 0 partitions the board by rows, and partition = 1 partitions the board by columns.