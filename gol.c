/**
 ** Game of Life
 ** 2D board of "live @" and "dead ." elements
 ** 
 ** Rules
 *** Live cell with 0 or 1 live neighbors dies
 *** Live cell with more than 4 neighbors dies
 *** Dead cell with 3 neighbors becomes alive 
 **/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>
#include <pthread.h>

int* getInitialConditions(char* argv[], int*rows, int*columns, int*num_iterations, int*num_initial_elements, int*display);
char* initializeBoard(int rows, int columns, int* indicies, int num_initial_elements);
void printBoard(int rows, int columns, char* game_board);
int countNeighbors(char* game_board, int rows, int columns, int i);
void liveOrDie(int num_neighbors, char* game_board, int rows, int columns, int i, int* kill, int* birth);
void updateBoard(int* kill, int*birth, char* game_board, int rows, int columns);
void timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y);
void runGame(int* num_iterations, char* game_board, int* rows, int* columns, int* kill, int*birth, int* display);

/*FOR PARALLEL*/
struct ThreadArgs{
    /*Data for particular thread*/
    int tid;
    int div_start;//ie from row/column x
    int div_end; //ie to row/column y
    int* thread_indicies; //array of indicies to own
    int size;
    
    /*Data each thread must access*/
    int num_iterations;
    char* game_board;
    int rows;
    int columns;
    int display;
    int* kill;
    int* birth;
};
typedef struct ThreadArgs ThreadArgs;

void* threadRoutine(void*arg);
void partitionRows(ThreadArgs* thread_args, int rows, int columns, int num_threads);
void partitionColumns(ThreadArgs* thread_args, int rows, int columns, int num_threads);
void print_column_allocation(ThreadArgs* thread_args, int num_threads, int rows, int columns);
void print_row_allocation(ThreadArgs* thread_args, int num_threads,int rows, int columns);

static pthread_barrier_t barrier;

int main(int argc, char *argv[]) {

    /**********************--------------------------***********************/
    /**********************    HANDLE CMD LINE ARGS  ***********************/
    /**********************--------------------------***********************/
    if(argc != 6){
        printf("Error on command line format \n");
        printf("Usage: ./gol board.txt print[0:1] num_threads partition[0:1] print_alloc[0:1]");
        exit(1);
    }
	if(strtol(argv[3], NULL, 10) <= 0) {
        printf("Must specify a positive integer for number of threads");
        exit(1);
    }
    if(strtol(argv[4], NULL, 10) != 0 &&  strtol(argv[4], NULL, 10) != 1){
        printf("Partition argument must be 0 or 1");
        exit(1);
    }
	if( strtol(argv[5], NULL, 10) != 0 &&  strtol(argv[5], NULL, 10) != 1){
        printf("Print allocation argument must be 0 or 1");
        exit(1);
    }
    
    /**********************--------------------------***********************/
    /**********************      PROGRAM VARIABLES   ***********************/
    /**********************--------------------------***********************/
    int rows;
    int columns;
    int num_iterations;
    int num_initial_elements;
    int display;

    /********************------------------------------*********************/
    /********************    THREAD PROGRAM VARIABLES  *********************/
    /********************------------------------------*********************/
    int num_threads = strtol(argv[3], NULL, 10);
    int row0_column1= strtol(argv[4], NULL, 10);
    int display_allocation = strtol(argv[5], NULL, 10);


    /********************------------------------------*********************/
    /********************       HANDLE THREADING       *********************/
    /********************------------------------------*********************/
    /*Initialize barrier with number of thread sthat will synchronize on it*/
    if(pthread_barrier_init(&barrier, NULL, num_threads)){
        perror("Error: with pthread barrier init");
        exit(1);
    }
    
    /*Store each thread data*/
    pthread_t *tids = malloc(sizeof(pthread_t)*num_threads);
    ThreadArgs *thread_args = malloc(sizeof(ThreadArgs)*num_threads);
    
    /********************------------------------------*********************/
    /********************   READ INFILE & INIT BOARD   *********************/
    /********************------------------------------*********************/
    /*Indicies = Index values for 1D array simulating coordinate pairs*/
    /*Read from file to get initial condition values*/
    int* indicies = getInitialConditions(argv, &rows, &columns, &num_iterations, &num_initial_elements, &display);
    if(indicies == NULL) {
        printf("Error on calloc");
        exit(1);
    }
    
    /********************------------------------------*********************/
    /********************          CREATE BOARD        *********************/
    /********************------------------------------*********************/
    char* game_board = initializeBoard(rows, columns, indicies, num_initial_elements);
    if(game_board== NULL){ 
        printf("Error on calloc"); 
        exit(1);
    }
    printBoard(rows,columns, game_board);
    
    int* kill = calloc(rows*columns, sizeof(int));
    int* birth = calloc(rows*columns, sizeof(int));
    if(kill == NULL || birth == NULL) {
        printf("Error on calloc");
        exit(1);
    }
    
    /********************--------------------------------*******************/
    /********************   PARTITION BOARD FOR THREADS  *******************/
    /********************--------------------------------*******************/
    /*Partition threads via rows or columns*/
    if(row0_column1 == 0)
        partitionRows(thread_args, rows, columns, num_threads);
    if(row0_column1 == 1)
        partitionColumns(thread_args, rows, columns, num_threads);
        
    /*Get start time*/
    struct timeval begin;
    int ret = gettimeofday(&begin, NULL);
    if(ret <0){
        printf("Error on gettimeofday \n");
        exit(0);
    }
    
    /********************--------------------------------*******************/
    /********************        PREPARE THREADS         *******************/
    /********************--------------------------------*******************/
    /*Give threads game args*/
    int t;
    for(t = 0; t<num_threads;t++){
        (*(thread_args + t)).num_iterations = num_iterations;
        (*(thread_args + t)).game_board = game_board;
        (*(thread_args + t)).rows = rows;
        (*(thread_args + t)).columns = columns;
        (*(thread_args + t)).display = display;
        (*(thread_args + t)).kill = kill;
        (*(thread_args + t)).birth = birth;
    }
    
    printf("INITIAL BOARD\n");
    printBoard(rows, columns, game_board);
    
    /********************--------------------------------*******************/
    /********************         CREATE THREADS         *******************/
    /********************            RUN GAME            *******************/
    /********************--------------------------------*******************/
    int w;
    for(w = 0; w < num_threads; w++){
        int ret = pthread_create(&tids[w], NULL, threadRoutine, &thread_args[w]);
        if(ret){
            perror("Error pthread_create"); 
            exit(1);
        }
    }
    if(display == 0) {
        printf("Final board:  \n");
        printBoard(rows,columns, game_board);
    }
    
    /********************--------------------------------*******************/
    /********************            CLEAN UP            *******************/
    /********************--------------------------------*******************/
    /*Join all threads*/
    int join;
    for(join = 0; join < num_threads; join++)
        pthread_join(tids[join], NULL);
        
    /*Get End Time*/
    struct timeval end;
    ret = gettimeofday(&end, NULL);
    if(ret <0){
        printf("Error on gettimeofday \n");
        exit(0);
    }
    
    /*Compute total time*/
    struct timeval result;
    timeval_subtract(&result, &end, &begin);
    printf("Run Time: %ld.%06ld seconds \n", result.tv_sec, result.tv_usec);
    
    if(display_allocation == 1){
        if(row0_column1 == 1) print_column_allocation(thread_args, num_threads, rows, columns);
        if(row0_column1 == 0) print_row_allocation(thread_args, num_threads,rows, columns);
    }
    
    /*Free memory resources*/
    free(kill);
    free(birth);
    free(indicies);
    free(game_board);
    
    pthread_exit(NULL);
    int destroy = pthread_barrier_destroy(&barrier);
    if(destroy !=0) {
        perror("Error on pthread_barrier_destroy \n");
        exit(1);
    }
    return 0;
}



/*Each thread holds an array of indicies it is responsible for
 * Per round, each thread does its partition of determining whether cells should live or die
 */
void* threadRoutine(void *arg){
    int tid = ((ThreadArgs *)arg)->tid;
    int*thread_indicies =((ThreadArgs *)arg)->thread_indicies;
    int size = ((ThreadArgs *)arg)-> size;
    
    int num_iterations = ((ThreadArgs *)arg)->num_iterations;
    char* game_board = ((ThreadArgs *)arg)->game_board;
    int rows = ((ThreadArgs *)arg)-> rows;
    int columns = ((ThreadArgs *)arg)->columns;
    int display = ((ThreadArgs *)arg)-> display;
    int* kill = ((ThreadArgs *)arg)-> kill;
    int* birth = ((ThreadArgs *)arg)->birth;
    
    int iteration = 0;
    int* num_neighbors_array = calloc(rows*columns, sizeof(int));
    
    int v;
    pthread_barrier_wait(&barrier);
    while(iteration < num_iterations){
        /*Store number of neighbors of each cell*/
        for(v = 0; v< rows*columns; v++){
            num_neighbors_array[v] = countNeighbors(game_board, rows, columns, v);
        }
        /*All threads wait so that they will read correct number of neighbors*/
        pthread_barrier_wait(&barrier);
        
        /*Each thread does its partition of determining whether cells live or die*/
        int i;
        for(i = 0; i<size; i++){
            liveOrDie(num_neighbors_array[thread_indicies[i]], game_board, rows, columns, thread_indicies[i], kill, birth);
        }
        
        /*All threads must finish determining cell fate before updating board*/
        pthread_barrier_wait(&barrier);
        updateBoard(kill, birth, game_board, rows, columns);
        
        /*Clear birth and kill arrays*/
        int z;
        pthread_barrier_wait(&barrier);
        for(z = 0; z<(rows*columns); z++){
            *(birth + z) = 0;
            *(kill + z) = 0;
        }
        
        /*Thread 0 does printing if display = 1*/
        pthread_barrier_wait(&barrier);
        if(tid == 0){
            if(display == 1){
                system("clear");
                printf("Time Step: %d \n", iteration+1);
                printBoard(rows,columns, game_board);
                usleep(150000);
            }
        }
        
        /*All threads must finish each turn before moving to next iteration*/
        pthread_barrier_wait(&barrier);
        iteration++;
    }
    
    free(num_neighbors_array);
    return NULL;
}



/*Partition grid row-wise*/
void partitionRows(ThreadArgs* thread_args, int rows, int columns, int num_threads){
    int i;
    int rows_per_thread = (int)(rows/num_threads);
    int num_rows_with_extra = rows%num_threads; 
    
    int row_index = 0;
    int from = 0;
    int to;
    
    int step =(rows_per_thread*columns);
    int step_with_extra = ((rows_per_thread+1)*columns);
    
    for(i = 0; i < num_threads-num_rows_with_extra; i++) {
        (*(thread_args + i)).div_start = row_index;
        (*(thread_args + i)).div_end = row_index + (rows_per_thread -1);
        
        (*(thread_args + i)).tid = i;
        to = from+step;
        
        (*(thread_args + i)).thread_indicies = calloc(to-from, sizeof(int));
        
        int j;
        int ti = 0;
        for(j = from; j < to; j++){
            (*(thread_args + i)).size = to-from;
            (*(thread_args + i)).thread_indicies[ti] = j;
            ti++;
        }
        
        from = to;
        row_index += rows_per_thread;
    }
    for(i = num_threads-num_rows_with_extra; i < num_threads; i++){
        (*(thread_args + i)).div_start = row_index;
        (*(thread_args + i)).div_end = row_index + (rows_per_thread);
        (*(thread_args + i)).tid = i;
        to = from+step_with_extra;
        
        (*(thread_args + i)).thread_indicies = calloc(to-from, sizeof(int));
        
        int j;
        int ti;
        for(j = from; j < to; j++){
            (*(thread_args + i)).size = to-from;
            (*(thread_args + i)).thread_indicies[ti] = j;
            ti++;
        }
        from = to;
        row_index = row_index + rows_per_thread + 1;
	}
}



/*Partition grid column wise*/
void partitionColumns(ThreadArgs* thread_args, int rows, int columns, int num_threads){

    int columns_per_thread = (int)(columns/num_threads);
    int num_columns_with_extra = columns%num_threads;
    
    int column_index = 0;
    int col_start;
    int col_end;
    
    int i;
    for(i = 0; i < num_threads-num_columns_with_extra; i++){
        col_start = column_index;
        col_end = column_index + (columns_per_thread -1);
        
        (*(thread_args + i)).div_start = col_start;
        (*(thread_args + i)).div_end = col_end;
        
        (*(thread_args + i)).tid = i;
        
        (*(thread_args + i)).thread_indicies = calloc(columns_per_thread*rows, sizeof(int));
        (*(thread_args + i)).size = columns_per_thread*rows;
        
        int j, k;
        int ti = 0;
        for(j = col_start; j<=col_end; j++){
            for(k = 0; k<rows*columns; k++){
                if(k%columns == j){
                    (*(thread_args + i)).thread_indicies[ti] = k;
                    ti++;
                }
            }
        }
        column_index += columns_per_thread;
    }
    
    for(i = num_threads-num_columns_with_extra; i < num_threads; i++){
        col_start = column_index;
        col_end = column_index + columns_per_thread;
        
        (*(thread_args + i)).div_start = col_start;
        (*(thread_args + i)).div_end = col_end;
        
        (*(thread_args + i)).tid = i;
        
        (*(thread_args + i)).thread_indicies = calloc((columns_per_thread+1)*rows, sizeof(int));
        (*(thread_args + i)).size =(columns_per_thread+1)*rows;
        
        int j, k;
        int ti;
        for(j = col_start; j<=col_end; j++){
            for(k = 0; k<rows*columns; k++){
                if(k%columns == j){
                    (*(thread_args + i)).thread_indicies[ti] = k;
                    ti++;
                }
            }
        }
        column_index = column_index + columns_per_thread + 1;
    }
}

/*Print allocation funciton for column wise partition*/
void print_column_allocation(ThreadArgs* thread_args, int num_threads, int rows, int columns){
    printf("Partitioning %d x %d board column-wise between %d threads \n", rows, columns, num_threads);
    int i;
    for(i = 0; i < num_threads; i++){
        printf("tid: %d: rows: 0 -> %d (%d) columns %d -> %d (%d) \n", (*(thread_args +i)).tid, rows-1, rows, (*(thread_args +i)).div_start, (*(thread_args +i)).div_end, ((*(thread_args +i)).div_end - (*(thread_args +i)).div_start)+1);
    }
}

/*Print allocation funciton for row wise partition*/
void print_row_allocation(ThreadArgs* thread_args, int num_threads,int rows, int columns){
    printf("Partitioning %d x %d board row-wise between %d threads \n", rows, columns, num_threads);
    int i;
    for(i = 0; i <num_threads;i++){
        printf("tid: %d, rows: %d -> %d (%d) columns 0 -> %d (%d) \n", (*(thread_args +i)).tid, (*(thread_args +i)).div_start, (*(thread_args +i)).div_end, (*(thread_args +i)).div_end - (*(thread_args +i)).div_start + 1, columns-1, columns);
    }
}


/* Subtract the �struct timeval� values X and Y, storing the result in RESULT.
 *  * @url https://www.gnu.org/software/libc/manual/html_node/Elapsed-Time.html
 *   *  */
void timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y){
        /* Perform the carry for the later subtraction by updating y*/
        if (x->tv_usec < y->tv_usec) {
                int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
                y->tv_usec -= 1000000 * nsec;
                y->tv_sec += nsec;
        }
        if (x->tv_usec - y->tv_usec > 1000000) {
                int nsec = (x->tv_usec - y->tv_usec) / 1000000;
                y->tv_usec += 1000000 * nsec;
                y->tv_sec -= nsec;
        }

        /* Compute the time remaining to wait.tv_usec is certainly positive*/
        result->tv_sec = x->tv_sec - y->tv_sec;
        result->tv_usec = x->tv_usec - y->tv_usec;
}



/*Reads from input file
 *Sets board parameters read from file via pointers
 *Returns array of "live" indicies for initial board
 *@param argv : command line arg
 *@param rows : # rows in game board
 *@param columns: #columns in game board
 *@param num_iterations: # times to run game
 *@param num_initial_elements: number of initial "live" elements
 *@param display: bit (0 or 1) indicating whether or not to display
 **      the board after each iteration
 *@return indicies: array of "live" indicies for initial board
 **/
int* getInitialConditions(char* argv[], int* rows, int* columns, int* num_iterations, int* num_initial_elements, int* display){
    char* filename = argv[1];
    FILE* infile;
    infile= fopen(filename, "r");
    if(infile == NULL){
        printf("Error: file open\n");
        exit(1);
    }
    
    fscanf(infile, "%d %d %d %d", rows, columns, num_iterations, num_initial_elements);
    
    int* coordinates = calloc(2*(*num_initial_elements), sizeof(int));
    int i;
    for(i = 0; i < 2*(*num_iterations); i++){
        int coordinate;
        if(fscanf(infile, "%d",&coordinate) == 1){
            *(coordinates + i) = coordinate;
        }
    }
    fclose(infile);
    
    int* indicies = calloc(*num_initial_elements, sizeof(int));
    if(coordinates == NULL || indicies == NULL){
        printf("Error on calloc"); 
        exit(1);
    }
    
    int j, k;
    for(j = 0, k = 0; j< *num_initial_elements; j++, k+=2){
        *(indicies + j) = (((*columns) * ((*(coordinates + k)))) + (*(coordinates + (k+1))));
    }
    free(coordinates);
    *display = strtol(argv[2], NULL, 10);
	return indicies;
}


/*
 ** Creates 1D array of characters to represent game board
 ** "live" elements are @
 ** "dead" elements are .
 ** @param rows: rows in game board
 ** @param columns: columns in game board
 ** @param indicies: int array storing indicies of "live" elements
 ** @param num_initial_elements: number of intial life elements
 ** @return game_board: 1D character array representing life board
 **/
char* initializeBoard(int rows, int columns, int* indicies, int num_initial_elements){
    char* game_board = malloc(rows*columns*sizeof(char));
    int i;
    for(i = 0; i< rows*columns; i++){
        *(game_board + i) = '.';
    }
    
    int j;
    for(j = 0; j<num_initial_elements; j++){
        *(game_board +  (*(indicies + j))) = '@';
    }
    return game_board;
}


/*Print 1D array as 2D board*/
void printBoard(int rows, int columns, char* game_board){
    int i;
    for(i = 0; i < rows*columns; i++){
        if(i%columns == 0)
            printf("\n");
        printf("%c ", *(game_board + i));
    }
    printf("\n");
}

/*Count number of live neighbors each cell has*/
int countNeighbors(char* game_board, int rows, int columns, int i){
    int n1, n2, n3, n4, n5, n6, n7, n8;
    n1 = n2 = n3 = n4 = n5 = n6 = n7 = n8 = 0;
    
    if( (i%columns != 0) && (((i+1) % columns) != 0) && (i > columns) && (i < (rows*columns - columns))){
        n1 = i + 1; 
        n2 = i - 1;
        n3 = i + columns;
        n4 = i - columns;
        n5 = i + columns + 1;
        n6 = i + columns - 1;
        n7 = i - columns + 1;
        n8 = i - columns - 1;
    }else if(i > 0 && i <(columns-1)) {
        n1 = i + 1;
        n2 = i - 1;
        n3 = i + columns;
        n4 = i + (columns*rows - columns);
        n5 = i + columns + 1;
        n6 = i + columns - 1;
        n7 = n4 +1;
        n8 = n4 -1;
    }else if(i> rows*columns - columns && i<rows*columns -1) {
        n1 = i + 1;
        n2 = i - 1;
        n3 = i - (columns*rows - columns);
        n4 = i - columns;
        n5 = n3 + 1;
        n6 = n3 - 1;
        n7 = i - columns + 1;
        n8 = i - columns - 1;
    }else if((i%columns == 0) && (i != 0) && (i!= rows*columns - columns)) {
        n1 = i + 1;
        n2 = i - 1;
        n3 = i + columns;
        n4 = i - columns;
        n5 = i + columns + 1;
        n6 = i + columns - 1;
        n7 = i - columns + 1;
        n8 = i -1 + 2*columns;
    }else if( ((i+1)% columns == 0) && (i != columns -1) && (i != rows*columns -1)) {
        n1 = i + 1;
        n2 = i - 1;
        n3 = i + columns;
        n4 = i - columns;
        n5 = i - 2*columns + 1;
        n6 = i + columns - 1;
        n7 = i - columns + 1;
        n8 = i - columns - 1;
    }
    
    if(i == 0){
        n1 = i + 1;
        n2 = i + 2*columns -1; 
        n3 = i + columns;
        n4 = (rows*columns) - columns + 1;
        n5 = i + columns + 1;
        n6 = i + columns - 1;
        n7 = (rows*columns) - columns; 
        n8 = (rows*columns) -1; 
	}
    if(i == columns -1) {
        n1 = i + 1;
        n2 = i - 1;
        n3 = i + columns;
        n4 = (rows*columns) - columns;
        n5 = (rows*columns) -1;
        n6 = i + columns - 1;
        n7 = 0;
        n8 = n5 - 1; 
    }
    if(i == (columns*rows - columns)) {
        n1 = i + 1;
        n2 = i - 1;
        n3 = 0;
        n4 = i - columns;
        n5 = columns -1; 
        n6 = i + (columns - 1);
        n7 = i - columns + 1;
        n8 = 1;
    }
    if(i == (rows*columns -1)) {
        n1 = i - 2*columns + 1;
        n2 = i - 1;
        n3 = 0;
        n4 = i - columns;
        n5 = columns -2;
        n6 = columns -1;
        n7 = i - columns + 1;
        n8 = i - columns - 1;
    }	

    int num_neighbors = 0;
    if(( *(game_board + n1)) == '@') {num_neighbors++;}
    if(( *(game_board + n2)) == '@') {num_neighbors++;}
    if(( *(game_board + n3)) == '@') {num_neighbors++;}
    if(( *(game_board + n4)) == '@') {num_neighbors++;}
    if(( *(game_board + n5)) == '@') {num_neighbors++;}
    if(( *(game_board + n6)) == '@') {num_neighbors++;}
    if(( *(game_board + n7)) == '@') {num_neighbors++;}
    if(( *(game_board + n8)) == '@') {num_neighbors++;}
    
    return num_neighbors;	
}


/*Implements live or die rules
 * Adds 1 to board game index location of array where cells need to be updated accordingly*/
void liveOrDie(int num_neighbors, char* game_board, int rows, int columns, int cell, int* kill, int* birth){
    if((*(game_board + cell)) == '@'){
        if((num_neighbors <= 1) || (num_neighbors >= 4)) {
            *(kill + cell) = 1;
        }
    }
    if( (*(game_board + cell)) == '.'){
        if((num_neighbors == 3)){
            *(birth + cell) = 1;
        }
    }
}	

/*Updates board*/
void updateBoard(int* kill, int*birth, char* game_board, int rows, int columns){
    int i;
    for(i = 0; i < rows*columns; i++){
        if( *(kill + i) == 1) {
            *(game_board + i) = '.';
        }
        if( *(birth + i) == 1){
            *(game_board + i) = '@';
        }
    }
}



